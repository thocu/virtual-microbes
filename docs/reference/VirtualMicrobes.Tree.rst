VirtualMicrobes.Tree package
============================

Submodules
----------

VirtualMicrobes.Tree.PhyloTree module
-------------------------------------

.. automodule:: VirtualMicrobes.Tree.PhyloTree
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: VirtualMicrobes.Tree
    :members:
    :undoc-members:
    :show-inheritance:
