VirtualMicrobes package
=======================

Subpackages
-----------

.. toctree::

    VirtualMicrobes.Tree
    VirtualMicrobes.cython_gsl_interface
    VirtualMicrobes.data_tools
    VirtualMicrobes.environment
    VirtualMicrobes.event
    VirtualMicrobes.mutate
    VirtualMicrobes.plotting
    VirtualMicrobes.post_analysis
    VirtualMicrobes.simulation
    VirtualMicrobes.virtual_cell

Module contents
---------------

.. automodule:: VirtualMicrobes
    :members:
    :undoc-members:
    :show-inheritance:
