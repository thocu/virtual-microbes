.. _glossary:

Glossary
========

.. glossary::
   :sorted:
   
   metabolite
      Any molecule that are not enzymes and that can undergo a reaction, 
      including influx and degradation reactions.
      
   building block
      A metabolite that is used by microbes to produce biomass. Biomass 
      production may depend on the simultaneous conversion of a set of 
      building blocks.
      
   LOD
      The line of descent is the genealogical succession of individuals 
      that connects some ancestor with one of its descendants. In the model
      these are constructed by storing all parent-offspring relations.
      



