VirtualMicrobes.virtual_cell package
====================================

Submodules
----------

VirtualMicrobes.virtual_cell.Cell module
----------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Cell
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.Chromosome module
----------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Chromosome
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.Gene module
----------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Gene
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.Genome module
------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Genome
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.GenomicElement module
--------------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.GenomicElement
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.Identifier module
----------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Identifier
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.PhyloUnit module
---------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.PhyloUnit
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.Population module
----------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Population
    :members:
    :undoc-members:
    :show-inheritance:

VirtualMicrobes.virtual_cell.Sequence module
--------------------------------------------

.. automodule:: VirtualMicrobes.virtual_cell.Sequence
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: VirtualMicrobes.virtual_cell
    :members:
    :undoc-members:
    :show-inheritance:
