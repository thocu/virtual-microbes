.. Virtual Microbes Evolutionary Simulator documentation master file, created by
   sphinx-quickstart on Thu Oct  6 09:58:46 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _contents:


VirtualMicrobes Evolutionary Simulator documentation
====================================================

Contents:

.. toctree::
   :maxdepth: 2
   
   overview
   install
   reference/index
   changelog
   contributing
   authors

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`glossary`

Project repository
==================

`on bitbucket <https://bitbucket.org/thocu/virtual-microbes/src/develop>`_
