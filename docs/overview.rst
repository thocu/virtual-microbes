********
Overview
********

The VirtualMicrobes software package is a simulator of Virtual Microbe evolution. Virtual Microbes 
live in spatial environment where they compete for resources. In order to grow and divide they express their
metabolic and regulatory genes to generate biomass. Mutations in their genome change the rates
of metabolic reactions and gene expression levels enabling them to become better adapted over time.

Motivation
----------

This software is being developed as part of the research in the `EvoEvo <http://www.evoevo.eu>`_ project.
The goal of the EvoEvo project is to understand how evolution can modify its own course. Mutations in 
the genome of an organism change the phenotype, thereby allowing adaptation to the environmental circumstances.
Yet, how genotypic changes are translated to phenotypic changes,
is itself an evolved property of species. Changing this genotype to phenotype mapping may make organisms more or less robust to mutations 
with respect to their functioning. Moreover, some structures of this mapping may make it more likely
for random mutations to have large phenotypic effects, increasing the chance that a novel adaptive 
phenotype will be discovered. The goal of the VirtualMicrobes project is to study the *in silico* 
evolution of genome structure and of the genotype to phenotype mapping.  

Features
--------

* population of individuals
* spatial environment    
* parameterized metabolic reaction space
* gene regulatory interaction network
* cell growth dynamics
* spatially extended genome
* genome and gene scale mutations
* lineage tracing
* ..